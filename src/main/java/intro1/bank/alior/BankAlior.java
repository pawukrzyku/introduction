package intro1.bank.alior;

import intro1.bank.Bank;

//6. Stworzyć pakiety: pl.sda.bank.alior oraz pl.sda.bank.ing.
//7. W pakiecie pl.sda.bank.alior stwórz klasę BankAlior, która ma dostęp publiczny i dziedziczy po klasie Bank.

public class BankAlior extends Bank {

//8. W klasie BankAlior dodaj pole o dostępie prywatnym przechowujące nazwę banku.

    private String bankName = "Bank Alior ";

//9. W klasie BankAlior dodaj metodę o dostępie prywatnym zwracającą prowizję jako wartość liczbową (prowizja to 10 + oprocentowanie).

    private double provision(){
        return (10 + valueOfPercentage());
    }

//10.W klasie BankAlior dodaj metodę o dostępie publicznym zwracającą nazwę banku wraz z prowizją.

    public String bankNameAndProvision(){
        return bankName + provision();
    }
}



