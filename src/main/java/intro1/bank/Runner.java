package intro1.bank;

//15.W pakiecie pl.sda.bank stwórz klasę Runner, która ma dostęp publiczny.

import intro1.bank.alior.BankAlior;
import intro1.bank.ing.BankING;

public class Runner {

//16.Dodaj metodę startową (psvm w IntelliJ).

    public static void main(String[] args) {

//17.Stwórz nową instancję klasy BankAlior i wypisz nazwę banku wraz z prowizją i oprocentowaniem.
//18.Stwórz nową instancję klasy BankING i wypisz nazwę banku wraz z prowizją i oprocentowaniem.

        BankAlior bankAlior = new BankAlior();
        BankING bankING = new BankING();

        System.out.println(bankAlior.bankNameAndProvision());
        System.out.println(bankAlior.valueOfPercentage());
        System.out.println(bankING.bankNameAndProvision());
        System.out.println(bankING.valueOfPercentage());

    }
}