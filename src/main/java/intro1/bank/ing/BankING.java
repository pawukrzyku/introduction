package intro1.bank.ing;

import intro1.bank.Bank;

//6. Stworzyć pakiety: pl.sda.bank.alior oraz pl.sda.bank.ing.
//11.W pakiecie pl.sda.bank.ing stwórz klasę BankING, która ma dostęp publiczny i dziedziczy po klasie Bank.

public class BankING extends Bank {

//12.W klasie BankING dodaj pole o dostępie prywatnym przechowujące nazwę banku.

    private String bankName = "Bank ING ";

//13.W klasie BankING dodaj metodę o dostępie prywatnym zwracającą prowizję jako wartość liczbową (prowizja to 15 + oprocentowanie).

    private double provision() {
        return (15 + valueOfPercentage());
    }

//14.W klasie BankING dodaj metodę o dostępie publicznym zwracającą nazwę banku wraz z prowizją.

    public String bankNameAndProvision(){
        return bankName + provision();
    }
}




