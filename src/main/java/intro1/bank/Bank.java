package intro1.bank;
//1.Stworzyć nowy projekt Maven dla aplikacji bankowej.
//2.Stworzyć pakiet:pl.sda.bank.
//3.W pakiecie pl.sda.bank stwórz klasę Bank,która ma dostęp publiczny.

public class Bank {

//4. W klasie Bank dodaj pole o dostępie prywatnym zawierającym oprocentowanie jakowartość liczbową.

    private int percentage;
    private int interest;

//5. W klasie Bank dodaj metodę o dostępie chronionym zwracającą wartość pola z oprocentowaniem.

    protected double valueOfPercentage() {
        return percentage;
    }

    public void setInterest(int interest) {
        this.interest = interest;
    }

}
/*
19.Jaka jest wartość oprocentowania (interest) w utworzonych obiektach?
20.W klasie Bank dodaj metodę o dostępie publicznym ustawiającą wartość oprocentowania zgodnie z przekazanym argumentem (public void setInterest(int interest)).
21.W klasie Runner, przed wywołaniem metod zwracających nazwę i prowizję, ustaw oprocentowanie na wybraną przez Ciebie wartość za pomocą metody z poprzedniego punktu. Uruchom program i porównaj rezultat.
*/