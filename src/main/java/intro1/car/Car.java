package intro1.car;

public class Car {
    String model;
    int maxSpeed;

    public Car(String model, int maxSpeed) {
        // this. odwołuje się do POLA!
        this.model = model;
        this.maxSpeed = maxSpeed;
    }

    void printCar() {
        System.out.println("Model samochodu: " + model + "; " + "Maksymalna prędkość: " + maxSpeed);
    }


}
