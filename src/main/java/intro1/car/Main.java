package intro1.car;

public class Main {
    public static void main(String[] args) {

        Car firstCar = new Car("Astra", 180);
        Car secondCar = new Car ("Omega", 290);

        firstCar.printCar();
        secondCar.printCar();
    }
}

/*
1. Dodaj do projektu z poprzedniego zadania klasę Car zawierającą:
1. Pole String model.
2. Konstruktor jednoargumentowy ustawiający pole model.
3. Metodę printCar() wyświetlającą model samochodu.
2. W metodzie main utwórz dwa obiekty Car (rożne modele).
3. Wywołaj metodę printCar() na obu obiektach. Uruchom program.
4. Dodaj pole int maxSpeed. Dodaj drugi argument do konstruktora
ustawiający to pole. Zmodyfikuj metodę printCar() aby wyświetlała
również maksymalną prędkość.
5. Utwórz dwa obiekty Car używając konstruktora dwuargumentowego.
6. Wywołaj metodę printCar() na obu obiektach. Uruchom program
 */