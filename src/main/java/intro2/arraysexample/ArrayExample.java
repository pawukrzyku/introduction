package intro2.arraysexample;

public class ArrayExample {

//SHIFT + F6 ZMIENIA NAZWE ZMIENNEJ, TAM GDZIE UZYTA TEZ ZMIENI NAZWE!
// MAVEN - NAZWY TESTOW MUSZA SIĘ ROZPOCZYNAĆ OD " test "

    int[] tab1 = new int[]{1, 2, 3, 4, 5,};
    int[] tab2 = {1, 2, 3, 4, 5};
    int[] tab3 = new int[5];

// NIE MOZNA USTAWIC POZ 3 W OGOLNEJ KLASIE MOZNA TO ZROBIC W KONSTRUKTORZE PONIŻEJ

    public ArrayExample() {
        this.tab3[3] = 8;
    }

    int[] tabWithoutValues = new int[5];
    String[] stringsWithoutValues = new String[5];
}

/*
1. Utworzyć nowy projekt Maven o nazwie arrays-example.
2. Stworzyć klasę ArrayExample.
3. W klasie ArrayExample stworzyć tablicę liczb całkowitych z pięcioma elementami (na czwartej pozycji ustaw wartość 8).
4. Stworzyć test dla klasy ArrayExample sprawdzający rozmiar tablicy.
5. Stworzyć test dla klasy ArrayExample sprawdzający czy element na pozycji czwartej to wartość 8.
6. W klasie ArrayExample zadeklarować tablicę (o nazwie tabWithoutValues) liczb całkowitych o rozmiarze pięć.
7. W klasie ArrayExample zadeklarować tablicę (o nazwie stringsWithoutValues) String o rozmiarze pięć.
8. Stworzyć test dla klasy ArrayExample sprawdzający element na indeksie 0 z tablicy tabWithoutValues.
9. Stworzyć test dla klasy ArrayExample sprawdzający element na indeksie 1 z tablicy stringsWithoutValues
 */