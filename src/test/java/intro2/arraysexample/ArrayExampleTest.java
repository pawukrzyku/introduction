package intro2.arraysexample;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

class ArrayExampleTest {

    public ArrayExample testArraysExample = new ArrayExample();

//    4. Stworzyć test dla klasy ArrayExample sprawdzający rozmiar tablicy.

    @Test
    public void testCheckingArraysLength() {
        assertThat(testArraysExample.tab3.length).isEqualTo(5);
        System.out.println("Rozmiar tablicy jest równy " + testArraysExample.tab3.length);
    }

    //    5. Stworzyć test dla klasy ArrayExample sprawdzający czy element na pozycji czwartej to wartość 8.

    @Test
    public void testCheckingValueOfPositionFour(){
        assertThat(testArraysExample.tab3[3]).isEqualTo(8);
        System.out.println("Wartość z pozycji 4 wynosi: " + testArraysExample.tab3[3]);
    }

//    8. Stworzyć test dla klasy ArrayExample sprawdzający element na indeksie 0 z tablicy tabWithoutValues.

    @Test
    public void testCheckingIndexZeroFromTabWithoutValues(){
        assertThat(testArraysExample.tabWithoutValues[0]).isEqualTo(0);
        System.out.println("Wartość na indeksie 0 z tabWithoutValues ma wartość: " + testArraysExample.tabWithoutValues[0]);
    }

//    9. Stworzyć test dla klasy ArrayExample sprawdzający element na indeksie 1 z tablicy stringsWithoutValues

    @Test
    public void testCheckingIndexOneFromTabWithoutValues(){
        assertThat(testArraysExample.stringsWithoutValues[1]).isEqualTo(null);
        System.out.println("Wartość na indeksie 1 z tabWithoutValues ma wartość: " + testArraysExample.stringsWithoutValues[1]);
    }

}